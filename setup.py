from setuptools import setup, find_packages

with open('README.md', 'r') as fh:
    long_description = fh.read()

setup(
    name='pydq',
    version='0.1.20',
    author='Andrew Freitas',
    author_email='andrewfreitas09@gmail.com',
    description='Python library for managing and persisting named Python Queues with a variety of backend choices',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/anfreitas/dq',
    packages=find_packages(),
    extras_require={"aws": ["boto3"], "pandas": ["pandas"]},
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)
